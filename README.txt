Script para escaneo utilizando NMAP parametros
Acepta los siguientes argumentos:

-t, --target, Recibe el dominio a consultar (Ejemplo: www.google.com)
-i, --input, Recibe un archivo con una lista de dominios o IPs a consultar, tambien se puede pasar una lista de IPs o dominios separados por espacios
-m, --method, Tipo de funcion a utilizar
-p, --ports, Puertos a consultar
-f, --format, Formato de salida (ports_status_service, ports, services) si no se especifica se imprime en formato JSON con toda la información
-a, --args Argumentos a utilizar insertalos de este modo ( sV sS ) (sin guiones)

Caracteristicas:
    * Se valida que se inserte al menos un target
    * Se valida que el dominio o la IP ingresada sea correcto
    * Se valida que los parametros -t -i no se ejecuten al mismo tiempo

    * Parametro -i permite combinar target por medio de la lectura de un archivo txt o target específico
        Ejemplo: -i hosts.txt www.cuevana.com 192.168.0.1


Se especifica los formatos de salida a partir de los parametros:
    * ports_status_service (imprime puertos, estado y servicio)
    * ports (imprime solo puertos)
    * services (imprime solo servicios)
    * Se imprime un json con todas la información disponible, si no se elije un formato de salida

Requerimientos:
    * Ejecutar script como root.
    * Se adjunta archivo requeriments.txt

Testeado en:
    * Python 3.9