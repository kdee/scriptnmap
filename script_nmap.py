#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import nmap3
import argparse
import re
import json
import validators
import os

regex = "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$"

parser = argparse.ArgumentParser(description="Programa para scanear hosts con nmap")
parser.add_argument("-t", "--target", help="Recibe el dominio a consultar (Ejemplo: www.google.com)")
parser.add_argument("-i", "--input", metavar='FILE', nargs='*', help="Recibe un archivo con una lista de dominios o IPs a consultar, tambien se puede pasar una lista de IPs o dominios separados por espacios")
parser.add_argument("-m", "--method", help="Tipo de funcion a utilizar")
parser.add_argument("-p", "--ports", help="Puertos a consultar")
parser.add_argument("-f", "--format", help="Formato de salida (ports_status_service, ports, services) si no se especifica se imprime en formato JSON con toda la informacion")
parser.add_argument("-a", "--args", nargs="+", help="Argumentos a utilizar insertalos de este modo ( sV sS )")

args = parser.parse_args()

#Valida que el argumento sea una IP o un dominio
def check(target): 
    if(re.search(regex, target)): 
        return True
    elif validators.domain(target):
        return True
    return False

#Imprime los resultados según el formato solicitado
def print_results(hosts, results, format):
    for key, value in results.items():
        for key2, value2 in value.items():
            if key2 == 'ports':
                print('Resultado del Escaneo al Host '+hosts+ ': \n')
                response = value2
                if format == 'ports_status_service':
                    for port in response:
                        print('Puerto: '+port['portid'] + '\tEstado: '+port['state'] + '\tServicio: '+port['service']['name'])
                elif format == 'ports':
                    for port in response:
                        print('Puerto: '+port['portid'])
                elif format == 'services':
                    for port in response:
                        print('Servicio: '+port['service']['name'])
      
            if key2 == 'ports'and value2 == []:
                json_formatted_str = json.dumps(results, indent=4)
                print(json_formatted_str)

#identifica el metodo a utilizar al momento de ejecutar el escaneo
def validate_method_scan(method):
    method_supported = ['scan_top_ports', 'nmap_detect_firewall', 'nmap_stealth_scan']
    if method not in method_supported:
        print('Método no soportado, intente con -m scan_top_ports , nmap_detect_firewall o nmap_stealth_scan')
        return None
    return method

def exec_method_scan(hs, method, arguments):

    scanner = nmap3.Nmap()
    print('Scaneando Host: '+hs)
    if method == 'scan_top_ports':
        results = scanner.scan_top_ports(hs, args=arguments)
    elif method == 'nmap_detect_firewall':
        results = scanner.nmap_detect_firewall(hs, args=arguments)
    elif method == 'nmap_stealth_scan':
        results = scanner.nmap_stealth_scan(hs, args=arguments)
    return results

#Ejecuta el escaneo y envia los resultados a la funcion print_results
def nmapScan(hosts, method, arguments, format, ports):
    
    method = validate_method_scan(method)

    if method is None:
        return None

    #Funcionalidad de multiples IPs o dominios
    if isinstance(hosts, list):
        for hs in hosts:
            results = exec_method_scan(hs, method, arguments)
            

    #Funcionalidad de solo una IP o dominio
    else:
        arguments = arguments + ports
        results = exec_method_scan(hosts, method, arguments)

  
    print_results(hosts, results, format)
    

#Lee targets de un archivo
def obtener_targets(target):    
    lst_host = []
    if not os.path.exists(target):
        lst_host.append(target.rstrip('\n'))
    else:
        with open(target) as f:
            for linea in f:
                lst_host.append(linea.rstrip('\n'))
    return lst_host


if __name__ == '__main__':

    arguments = args.args if args.args else ''
    format = args.format if args.format else ''
    ports = '-p '+args.ports if args.ports else ''
    
    ls = ''
    if arguments:
        for arg in arguments:
            arguments = ls + ' -' + arg
        arguments = ls

    #Validacion de los argumentos ingresados -t o -i al momento de ejecutar el escaneo
    if args.input and args.target:
        print('No puede utilizar los parametros -i y -t a la vez')

    if args.input:
        hosts = args.input
        if ' ' in hosts:
            hosts = hosts.split(' ')
        for host in hosts:
            hosts = obtener_targets(host)
            if check(host):
                for x in hosts:
                    nmapScan(x, args.method, arguments, format, ports)
            else:
                print('El dominio o IP '+host+' no es valido')
                continue

    elif args.target:
        host = args.target
        if not check(host):
            print('El host o dominio no es valido')
            exit()
        nmapScan(host, args.method, arguments, format, ports)
